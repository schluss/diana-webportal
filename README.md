![DIANA project - webportal](schluss-diana-banner.png "DIANA project - webportal")

# DIANA project - webportal

Demonstration webportal, making it possible to have a guided flow for the DIANA project. Use in conjunction with the Schluss (https://gitlab.com/schluss/beta-app) app

## Requirements
- NodeJS

## Run locally
Clone the repository, rename the .env.example file to .env and edit the contents where needed. Then:


### Install packages
To install all the required packages, run:
> npm install


### Local webportal
To run the webportal locally, run: 
> npm run dev


## License
This project is licensed with the BSD 3-Clause License.
