"use strict";

const fs = require('fs');
const express = require('express');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const path = require('path');
const app = express();
const { v4: uuidv4 } = require('uuid');

const jsonParser = bodyParser.json({type: 'application/json', limit : '256kb'});

// log all requests
app.use((req, res, next) => {
	console.log('['+ req.method + '] ' + req.url);
	return next();
});	

// connect static assets 
app.use(express.static(path.join(__dirname, 'static')));


// in memory data storage - for demonstration purposes
let memory = [];

const storage = {
	
	exists : function(id){
		if (memory[id]){
			return true;
		}
		
		return false;
	},
	
	
	get :  function (id){
		if (!storage.exists(id)){
			return '';
		}
		
		return memory[id];
	},
	
	set : function(id, data){
		
		data['timestamp'] = Math.round(Date.now() / 1000); // timestamp in seconds since epoch
		
		memory[id] = data;
		
	},
	
	// remove items older than timeout (in seconds) 
	clean : function(timeout = 1800){
		let time = Math.round(Date.now() / 1000) - timeout;

		for (const i in memory){
			
			
			if (memory[i].timestamp < time){
				delete memory[i];
			}
		}
		
		console.log('memory cleanup done');
	}
	
};

// run memory cleanup as a 10min interval
let interval = setInterval(() => {
	storage.clean()
}, 600000);


// routes
app.get('/', (req, res) =>{
	return res.render('index', {
		title : 'Doorgeven gegevens',
		bankUrl : process.env.BANK_URL,
		sessionId : uuidv4()
	});	
});



app.get('/config.json', (req, res) =>{

	let json = fs.readFileSync(path.join(__dirname, 'config.json'), 'utf8');
	
	json = json.replaceAll('[bankurl]', process.env.BANK_URL);
	json = json.replaceAll('[notaryurl]', process.env.NOTARY_URL);
	json = json.replaceAll('[governmenturl]', process.env.GOVERNMENT_URL);

	res.setHeader('Content-Type', 'application/json');
	return res.json(JSON.parse(json));	 
});


// API implementation
// recieve the requested data
app.post('/api/incoming', jsonParser, (req, res) =>{
	res.setHeader('Content-Type', 'application/json');
	
	const token = req.headers['x-access-token'] || req.headers.authorization;
	
	if (!storage.exists(token)){
		return res.json({'message':'invalid token'});
	}

	let obj = storage.get(token);
	
	// update state and store contract
	obj['state'] = 'done';
	obj['contract'] = req.body;
	
	return res.json({'message':'contract received'});
	
});

// recieve status updates about the datarequest progression from the client
app.post('/api/status', jsonParser, (req, res) => {
	res.setHeader('Content-Type', 'application/json');
	
	const token = req.headers['x-access-token'] || req.headers.authorization;
	const body = req.body;
	
	if (!storage.exists(token)){
		return res.json({'message':'invalid token'});
	}
	
	let obj = storage.get(token);
	
	// update state
	obj['state'] = body.state;

	storage.set(token, obj);
	
	return res.json({'message':'status updated'});
	
});

// check the status of receiving requested data, when received, it is returned
app.get('/api/check', (req, res) => {
	res.setHeader('Content-Type', 'application/json');
	const token = req.headers['x-access-token'] || req.headers.authorization;
	
	console.log('token'+ token);
	
	// token not stored yet, create a new memory storage item on the fly
	if (!storage.exists(token)){	
		storage.set(token, {'state' : 'new'});
	}
	
	let obj = storage.get(token);
	
	console.log(obj);
	
	return res.json({'state': obj.state});
	
});

// handle errors
app.use(function (err, req, res, next) {
	
	if (res.headersSent) {
		return next(err);
	}
  
	return res.status(500).send(err.message);
});

// template engine
app.engine('html', exphbs.engine({
	extname: '.html',
	defaultLayout : 'main'
}));

app.set('view engine', 'html');

// set the views location
app.set('views', path.resolve(__dirname, './views'));

	
module.exports = app;