"use strict";

let BASEURL = process.env.BASE_URL;


const fs = require('fs');
const express = require('express');
const exphbs = require('express-handlebars');
const helmet = require('helmet');
const path = require('path');
const app = express();

// log all requests
app.use((req, res, next) => {
	console.log('['+ req.method + '] ' + req.url);
	return next();
});	

// connect static assets 
app.use(express.static(path.join(__dirname, 'static')));

// routes
app.get('/', (req, res) =>{
	return res.render('index', {
		title : 'Schluss DIANA webportal',
		baseurl : BASEURL,
		governmentUrl : process.env.GOVERNMENT_URL,
		insurerUrl : process.env.INSURER_URL,
		notaryUrl : process.env.NOTARY_URL,
		bankUrl : process.env.BANK_URL
	});	 
});

app.get('/config.json', (req, res) =>{
	
	
	let json = fs.readFileSync(path.join(__dirname, 'config.json'), 'utf8');
	
	json = json.replaceAll('[baseurl]', BASEURL);
	json = json.replaceAll('[governmenturl]', process.env.GOVERNMENT_URL);

	res.setHeader('Content-Type', 'application/json');
	return res.json(JSON.parse(json));	 
});


// handle errors
app.use(function (err, req, res, next) {
	
	if (res.headersSent) {
		return next(err);
	}
  
	return res.status(500).send(err.message);
});

// template engine
app.engine('html', exphbs.engine({
	extname: '.html',
	defaultLayout : 'main'
}));

app.set('view engine', 'html');

// set the views location
app.set('views', path.resolve(__dirname, './views'));

	
module.exports = app;